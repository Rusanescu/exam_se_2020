package ex2;

public class Main {

    public static void main(String[] args) {

        IThread1 th1 = new IThread1();
        IThread2 th2 = new IThread2();

        th1.start();
        th2.start();
    }

    private static class IThread1 extends Thread{
        private int index = 0;

        public IThread1() {
        	index = 1;
        }

        public void run(){
            try {
                while(index <= 4) {
                    System.out.println("IThread1  - " + index);
                    Thread.sleep(10000);
                    index++;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static class IThread2 extends Thread{
        private int index = 0;

        public IThread2() {
        	index = 1;
        }

        public void run(){
            try {
                while(index <= 4) {
                    System.out.println("IThread2  - " + index);
                    Thread.sleep(10000);
                    index++;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}