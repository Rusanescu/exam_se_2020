package ex1;

public class main {

    private interface U{

    }

    private class I implements U{
        private long t;
        private K k;

        public void f(){

        }

        public void i(J j){

        }

    }

    private class J{

    }

    private class K{
        private L l;
        private S s;
    }

    private class L{

        public void metA(){

        }

    }

    private class N{
        private I i;
    }

    private class S{

        public void metB(){

        }
    }

}